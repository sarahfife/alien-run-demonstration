﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace AlienRun
{
    class Level
    {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D bridgeTexture;
        private Texture2D spikeTexture;
        private Texture2D flagTexture;
        private Texture2D bronzeCoinTexture;
        private Texture2D silverCoinTexture;
        private Texture2D goldCoinTexture;

        private bool completedLevel = false;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;

        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGT = 70;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            // Creating a single copy of the tile texture that will be used by all tiles
            boxTexture = content.Load<Texture2D>("graphics/tiles/box");
            bridgeTexture = content.Load<Texture2D>("graphics/tiles/bridge");
            spikeTexture = content.Load<Texture2D>("graphics/tiles/spikes");
            flagTexture = content.Load<Texture2D>("graphics/tiles/goal");
            bronzeCoinTexture = content.Load<Texture2D>("graphics/items/coinBronze");
            silverCoinTexture = content.Load<Texture2D>("graphics/items/coinSilver");
            goldCoinTexture = content.Load<Texture2D>("graphics/items/coinGold");

            SetupLevel();
        }
        // -------------------------------------
        public void SetupLevel()
        {
            completedLevel = false; 

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            // Create solid boxes
            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);

            // Create platforms
            CreateBridge(1, 4);
            CreateBridge(2, 4);
            CreateBridge(6, 6);
            CreateBridge(7, 6);
            CreateBridge(8, 6);
            CreateBridge(9, 6);

            // Create spikes
            CreateSpikes(9, 10);
            CreateSpikes(10, 10);
            CreateSpikes(11, 10);
            CreateSpikes(12, 10);
            CreateSpikes(13, 10);

            // Create Coins
            CreateBronzeCoin(1, 3);
            CreateBronzeCoin(2, 3);
            CreateSilverCoin(6, 5);
            CreateSilverCoin(7, 5);
            CreateGoldCoin(8, 5);

            // Create the goal
            CreateFlag(9, 5);
        }
        // -------------------------------------
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(boxTexture, tilePosition, Tile.TileType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateSpikes(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(spikeTexture, tilePosition, Tile.TileType.LETHAL);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateFlag(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(flagTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(bronzeCoinTexture, tilePosition, Tile.TileType.POINTS, 1);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(silverCoinTexture, tilePosition, Tile.TileType.POINTS, 5);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(goldCoinTexture, tilePosition, Tile.TileType.POINTS, 10);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        // -------------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGT) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Tile thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // -------------------------------------
        public Tile GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            // Otherwise we are within the bounds
            return tiles[x, y];
        }
        // -------------------------------------
        public void CompleteLevel()
        {
            completedLevel = true;
        }
        // -------------------------------------
        public bool GetCompletedLevel()   // IsLevelComplete()
        {
            return completedLevel;
        }
        // -------------------------------------
    }
}
